class levels.PortalData
{

	var mc		: MovieClip;
	var cx		: Int ;
	var cy		: Int ;

	var x		: Float; // for animation purpose only
	var y		: Float;
	var cpt		: Float;


	/*------------------------------------------------------------------------
	CONSTRUCTEUR
	------------------------------------------------------------------------*/
	function new( mc, cx:Int,cy:Int ) {
		this.mc = mc;
		this.cx = cx ;
		this.cy = cy ;

		x = mc._x;
		y = mc._y;
		cpt = 0;
	}

}


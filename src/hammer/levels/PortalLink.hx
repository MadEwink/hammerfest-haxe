class levels.PortalLink
{

	// from
	var from_did	: Int;
	var from_lid	: Int;
	var from_pid	: Int;

	// to
	var to_did		: Int;
	var to_lid		: Int;
	var to_pid		: Int;



	function new() {
	}


	function cleanUp() {
		if ( Std.isNaN(from_pid) )  {
			from_pid = 0;
		}
		if ( Std.isNaN(to_pid) )  {
			to_pid = 0;
		}
	}


	function trace() {
		Log.trace("link: "+from_did+","+from_lid+"("+from_pid+")  > "+to_did+","+to_lid+"("+to_pid+")");
	}
}


class levels.Data
{
	var $map			: Array<Array<Int>>;
	var $badList		: Array< levels.BadData >;

	var $playerX		: Int;
	var $playerY		: Int;

	var $skinTiles		: Int;
	var $skinBg			: Int;

	var $specialSlots	: Array< {$x:Int,$y:Int} >;
	var $scoreSlots		: Array< {$x:Int,$y:Int} >;

	var $script			: String;


	/*------------------------------------------------------------------------
	CONSTRUCTEUR
	------------------------------------------------------------------------*/
	function new() {
		$map = new Array();
		for ( x in 0...Data.LEVEL_WIDTH ) {
			$map[x] = new Array();
			for ( y in 0...Data.LEVEL_HEIGHT ) {
				$map[x][y] = 0;
			}
		}

		$playerX		= 0;
		$playerY		= 0;
		$skinBg			= 1;
		$skinTiles		= 1;
		$badList		= new Array();

		$specialSlots	= new Array();
		$scoreSlots		= new Array();

		$script			= "";
	}
}


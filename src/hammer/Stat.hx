class Stat
{
	var current : Float ;
	var total : Float ;


	/*------------------------------------------------------------------------
	CONSTRUCTEUR
	------------------------------------------------------------------------*/
	function new() {
		current = 0 ;
		total = 0 ;
	}


	/*------------------------------------------------------------------------
	OPÉRATIONS COURANTES
	------------------------------------------------------------------------*/
	function inc(n) {
		current+=n ;
	}


	function reset() {
		total+=current ;
		current = 0 ;
	}
}

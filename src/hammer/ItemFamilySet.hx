interface ItemFamilySet {
	var id		: Int; // id (frame-1)
	var r		: Int; // rarity
	var v		: Int; // value (score)
	var name	: String;
}

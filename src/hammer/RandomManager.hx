class RandomManager
{
	var bulks		: Array<Array<Int>>;
	var expanded	: Array<Array<Int>>;

	var sums		: Array<Int>;


	/*------------------------------------------------------------------------
	CONSTRUCTEUR
	------------------------------------------------------------------------*/
	function new() {
		bulks		= new Array();
		expanded	= new Array();
		sums 		= new Array();
	}


	/*------------------------------------------------------------------------
	AJOUTE UN TABLEAU
	------------------------------------------------------------------------*/
	function register(id, bulk) {
		bulks[id] = bulk;
		computeSum(id);
//		expand(id);
	}


	/*------------------------------------------------------------------------
	COMPUTE SUM OF ALL ELEMENTS IN A BULK ARRAY
	------------------------------------------------------------------------*/
	function computeSum(id) {
		sums[id]=0;
		for ( i in 0...bulks[id].length ) {
			if ( bulks[id][i]==null ) {
				bulks[id][i]=0;
			}
			sums[id] += bulks[id][i];
		}
	}


	/*------------------------------------------------------------------------
	CRÉATION DU CACHE
	------------------------------------------------------------------------*/
	function expand(id) {
		expanded[id] = new Array();
		for ( i in 0...bulks[id].length ) {
			for ( j in 0...bulks[id][i] ) {
				expanded[id].push(i);
			}
		}
	}


	/*------------------------------------------------------------------------
	TIRAGE
	------------------------------------------------------------------------*/
	function draw(id) {
		// light system
		var tab		= bulks[id];
		var i		= 0;
		var target	= Std.random(sums[id]);
		var sum		= 0;
		var result	= null;
		while (i<tab.length && result==null) {
			sum+=tab[i];
			if ( target<sum ) {
				result = i;
			}
			i++;
		}

		if ( result==null ) {
			GameManager.warning("null draw in array "+id);
		}

		return result;

		// deprecated expanded system
//		return expanded[id][ Std.random(expanded[id].length) ];
	}


	/*------------------------------------------------------------------------
	RENVOIE LES CHANCES DE TIRER LA VALEUR DONNÉE (ratio / 1)
	------------------------------------------------------------------------*/
	function evaluateChances(id:Int,value:Int) {
//		var chance	= 1;
//		var tab		= bulks[id];
//		var i		= 0;
//		var sum		= 0;
//		var total	= sums[id];
		Log.trace("item="+value);
		Log.trace(bulks[id][value]);
		Log.trace(sums[id]);
		return bulks[id][value] / sums[id];
	}


	function remove(rid:Int, id:Int) {
		bulks[rid][id] = 0;
		computeSum(rid);
	}



	function drawSpecial() {
		//
	}
}

# Hammerfest Haxe Project

## What is Hammerfest ?

Hammerfest is a video game developped by Motion Twin that came out in february 2006.

It is now distributed with its source code under a CC BY-NC-SA 4.0 license, see the [github repo](https://github.com/motion-twin/hammerfest).

The game was developped for Flash using the internal MotionTypes (mtypes) language.

## What is this project ?

This project is a humble attempt to transfer the source code from its original language MotionTypes, to Haxe.

Haxe being the successor of MotionTypes, there seem to be many similarities between the two, making the transfer look reasonable to me.

This would make it possible to compile Hammerfest to quite any platform.

## How to compile ?

The project is under constuction, and unlike other projects, it will not really compile until it's almost over.

For now, there are a lot of compilation error resulting from the differences between Haxe and MotionTypes syntaxes.

If you wish to see the compilation errors, you can compile any file using haxe.

I'll try to write proper compilation instruction, including a list of dependencies and everything if this project makes it way to a viable state.

For now I'll just tell you to [learn about haxe here](https://haxe.org/) if you are new to it.
